const {Command} = require('../../../src/Server')

class StreamCommand extends Command {
    constructor(fleewjsWebcam) {
        super('stream', [], 'Command to manage webcam streaming', '/stream [createScreen|start|stop|pause]')
        this.fleewjsWebcam = fleewjsWebcam
    }
    execute(sender, args, alias) {
        if(!sender._client) {
            sender.sendMessage('§4You must be in game to run this command')
            return true
        }
        if('createScreen start stop pause'.indexOf(args[0]) == -1) return false
        return true
    }
}

module.exports = {StreamCommand}