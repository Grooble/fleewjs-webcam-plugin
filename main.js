const {Plugin} = require('../../src/Server')
const {StreamCommand} = require('./src/commands')

class FleewjsWebcam extends Plugin {
    onEnable() {
        console.log('hey');
        this.registerCommand(new StreamCommand(this))
    }
    onDisable() {
        
    }
}

module.exports = FleewjsWebcam
